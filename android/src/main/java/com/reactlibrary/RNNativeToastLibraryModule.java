
package com.reactlibrary;
import android.content.Context;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;


// package com.firebase.firestore.counter;

import androidx.annotation.Nullable;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.SetOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;


public class RNNativeToastLibraryModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;


  private static final String SHARD_COLLECTION_ID = "_counter_shards_";

  private final DocumentReference doc;
  private final String field;
  private final FirebaseFirestore firestore;
  private final String shardId;
  private final Map<String, Double> shards = new HashMap<>();

  public RNNativeToastLibraryModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }



  @Override
  public String getName() {
    return "RNNativeToastLibrary";
  }

  @ReactMethod
  public void show(String text) {
    Context context = getReactApplicationContext();
    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
  }
  @ReactMethod
  public void show2(String text) {
    Context context = getReactApplicationContext();
    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
  }


  @ReactMethod
  public Task<Void> incrementBy(double value) {
    FieldValue increment = FieldValue.increment(value);
    String[] parts = field.split("\\.");

    Map<String, Object> update = null;
    for (int i = parts.length - 1; i >= 0; i--) {
      String part = parts[i];
      Map<String, Object> innerUpdate = new HashMap<>();
      if (update == null) {
        innerUpdate.put(part, increment);
      } else {
        innerUpdate.put(part, update);
      }
      update = innerUpdate;
    }
    try {
      return shard().set(update, SetOptions.merge());
    } catch (Exception e) {
      return null;
      //TODO: handle exception
    }

  }

  public FirestoreShardedCounter(DocumentReference doc, String field) {
    this.doc = doc;
    this.field = field;
    this.firestore = doc.getFirestore();
    this.shardId = UUID.randomUUID().toString();

    CollectionReference shardsRef = doc.collection(SHARD_COLLECTION_ID);
    this.shards.put(doc.getPath(), 0D);
    this.shards.put(shardsRef.document(this.shardId).getPath(), 0D);
    this.shards.put(shardsRef.document("\t" + this.shardId.substring(0, 4)).getPath(), 0D);
    this.shards.put(shardsRef.document("\t\t" + this.shardId.substring(0, 3)).getPath(), 0D);
    this.shards.put(shardsRef.document("\t\t\t" + this.shardId.substring(0, 2)).getPath(), 0D);
    this.shards.put(shardsRef.document("\t\t\t\t" + this.shardId.substring(0, 1)).getPath(), 0D);
  }


  public DocumentReference shard() {
    return doc.collection(SHARD_COLLECTION_ID).document(shardId);
  }

}